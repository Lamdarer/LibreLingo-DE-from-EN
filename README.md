# LibreLingo-DE-from-EN

LibreLingo [German course](https://librelingo.app/course/german-from-english) for English speakers

<p align="center">
 <h1 align="center">LibreLingo</h1>
 <p align="right"><em>a community-owned language-learning platform</em></p>
 <h4 align="center">»<a href="https://librelingo.app/">&nbsp;Try&nbsp;LibreLingo&nbsp;now!&nbsp;</a>«</h4>
</p>

<p align="center">
  <a href="https://matrix.to/#/+librelingo:matrix.org">💬 Chat</a> •
  <a href="https://github.com/kantord/LibreLingo/blob/main/README.md#become-a-contributor>👩‍💻 Contribute!</a> •
  <a href="https://github.com/kantord/LibreLingo">💵 Sponsor</a>  •
  <a href="https://librelingo.app/docs/">📄 Development docs</a>  •
  <a href="https://github.com/kantord/LibreLingo#readme">☎️ Schedule a call about Librelingo</a>
</p>



## About LibreLingo

LibreLingo's mission is to create a modern language-learning platform that is owned by the community of its users. All software is licensed under AGPLv3, which guarantees the freedom to run, study, share, and modify the software. Course authors are encouraged to release their courses with free licenses.

 If you want to know why Kantord built LibreLingo, [I recommend reading Kantord's article](https://dev.to/kantord/why-i-built-librelingo-280o). If you agree with LibreLingo's mission, [consider helping to build and maintain it](https://github.com/kantord/LibreLingo/blob/main/README.md#become-a-contributor), and [please consider making a monetary contribution](https://github.com/kantord/LibreLingo).
